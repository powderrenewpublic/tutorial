Getting Help
============

Because of the number of participants, we have a number of staff standing by on a chat server to offer assistance as you go through this tutorial. If something doesn't make sense or you run into any issue, log into our temporary server and ask for help.

[Live Helpline](https://pc201.emulab.net/signup_user_complete/?id=1sgxbn1zdpdrmy7t94119bucua)

*When you ask for help, make sure to include your username and a link to your profile or experiment.*

Log in to the POWDER portal
===============================

1.  Point your browser at the POWDER portal:

	[https://www.powderwireless.net](https://www.powderwireless.net)

2.	Log in with the username and password provided on the handout.

"Hello World" -- Using GNU Radio
================================

1.  Find the experiment started on your behalf (use the `Experiments` menu `My Experiments` page and click on the Name of the experiment). On the `Topology View` tab in your experiment view, click on the only node in your experiment (`pnode`) and select `Shell` from the pop-up menu to open a browser shell.  Within that shell, execute:
    ```
    /usr/local/powder/runvnc.sh
    ```
    You can reuse your login password for the requested password.
    
1.  Open the link given in the shell, of the form shwon below (i.e., use the link that looks like  this *in your experiment*, not the one below):

    https://pnode.foo.sigcomm2019.emulab.net:8787/vnc_auto.html

    You will probably have to persuade your browser to accept the node's self-signed certificate. And you will have to re-enter the password you provided.

1.  Within a terminal in the resulting VNC desktop, execute:
    ```
    gnuradio-companion /usr/local/powder/tutorial/psk.grc
    ```
    You will be able to manipulate and run the GNU Radio Companion flow graph on the remote system.

1.  When you are finished, return to the experiment page and press the `Terminate` button.

End-to-end mobile network - EPC and RAN using Open Air Interface
=================================================================

1.  Find the `sigcomm2019` profile from the POWDER portal.  (Use the `Experiments` menu `Start Experiment` entry, select `Change Profile`, then search for/change to the `sigcomm2019` profile.)

    Rather than instantiating the profile, make your own copy by selecting the `Copy Profile` option.  Use your
    username as the profile name (to prevent collisions).

1. Modify your copy of the profile as follows:

    a) Delete the line specifying the GNU Radio disk image:  
    ~~`pnode.disk_image = GLOBALS.GNURADIO_IMG`~~
    
    
    b) Use the functions and definitions provided to create the `sim_ran` VM, immediately after where the `pnode`
    is created and where the disk image assignment used to be:
    ```
    sim_ran = mkVM("sim-ran", GLOBALS.OAI_SIM_IMG)
    ```
    
    c) Use the functions and definitions provided to create the `epc` VM, immediately after the `sim_ran` one:

    ```
    epc = mkVM("epc", GLOBALS.OAI_EPC_IMG)
    ```
    
    d) Add a start-up script for the `epc` VM:
    ```
    epc.addService(rspec.Execute(shell="sh", command=GLOBALS.OAI_CONF_SCRIPT + " -r EPC"))
    ```
    
    e) Add the following statements to request a network link connecting the two VM nodes:
    ```
    link = request.Link("s1-lan")
    link.trivial_ok = True
    link.addNode(sim_ran)
    link.addNode(epc)
    ```

1. Click `Accept` and then `Create` to save your new profile. (Or if you did `Create` previously, you would do `Accept` and then `Save`.) 

1. `Instantiate` your new profile.  On the `Schedule` page, stay with the default options and select `Finish`.

1. Once your experiment instantiated successfully, there will be a physical node (`pnode`) containing two VM nodes, `sim-ran` and `epc`, in the topology.


1. Start the Open Air Interface (OAI) mobile networking components.

	(a) Open a shell on the `epc` node and start up the Open Air Interface (OAI) EPC:

	```
	sudo /local/repository/bin/start_oai.pl;
	tail -f /var/log/oai/mme.log;
	```

	(b) Open a shell on the `sim-ran` node and start up the simulated OAI eNodeB:

	```
	cd /opt/oaisim-xran/cmake_targets/lte_build_oai/build;
	sudo RFSIMULATOR=enb ./lte-softmodem -O /opt/oaisim-xran/enb1.conf --rfsim;
    ```

	If you want to restart the eNodeB: Type "Control-C" to kill the process, then execute the last command again to restart.

	(c) Open another shell on the `sim-ran` node and start up the simulated OAI UE:

	```
	cd /opt/oaisim-xran/cmake_targets/lte_build_oai/build;
	sudo RFSIMULATOR=127.0.0.1 ./lte-uesoftmodem -C 2125000000 -r 25 --rfsim;
	```

	If you want to restart the UE: Type "Control-C" to kill the process, then execute the last command again to restart.

1.  At this point the EPC output should show the eNodeB and UE connected.

1.  Verify the mobile network setup. Open another shell on the `sim-ran`
node and execute:

	```
	ping -I oaitun_ue1 8.8.8.8
	```
	
1.  Close all shell tabs, but leave the experiment running. (I.e., do *not* terminate the experiment.)


RAN softwarization - Using OAI, XRAN and the ONOS framework
============================================================

1.  Open another shell on the `epc` node. Refresh and run the ONOS controller:

	(a) Source the ONOS environment etc.:

	```
	sudo bash --rcfile /opt/oai/shell-onos
	```
	
	(b) Refresh the ONOS build:
	```
	onos-buck build onos --show-output
	```
	
	(c) Run ONOS:
	```
	onos-buck run onos-local
	```
	
1.  Once the ONOS controller started up, open another shell on the `epc` noded and start the ONOS console:

	(a) Source the ONOS environment etc.:

	```
	sudo bash --rcfile /opt/oai/shell-onos
	```
	
	(b) Register the eNodeB agent with XRAN:
	```
	onos-netcfg 127.0.0.1 apps/xran/xran-cfg.json
	```
	
	(c) Start up the ONOS console:
	```
	onos 127.0.0.1 -l onos
	```

	The password is: rocks

1.  Within the ONOS console activate the XRAN component:

	```
	app activate org.onosproject.xran
	```
	
1.  Verify that XRAN has no UEs associated with it. Within the ONOS console run:

	```
	hosts
	```
	
	At this point this should *not* show any output.

1. Restart the OAI components.

	(a) Open a shell on the `epc` node and start up the Open Air Interface (OAI) EPC:

	```
	sudo /local/repository/bin/start_oai.pl;
	tail -f /var/log/oai/mme.log;
	```

	(b) Open a shell on the `sim-ran` node and start up the simulated OAI eNodeB:

	```
	cd /opt/oaisim-xran/cmake_targets/lte_build_oai/build;
	sudo RFSIMULATOR=enb ./lte-softmodem -O /opt/oaisim-xran/enb1.conf --rfsim;
    ```

	If you want to restart the eNodeB: Type "Control-C" to kill the process, then execute the last command again to restart.

	(c) Open another shell on the `sim-ran` node and start up the simulated OAI UE:

	```
	cd /opt/oaisim-xran/cmake_targets/lte_build_oai/build;
	sudo RFSIMULATOR=127.0.0.1 ./lte-uesoftmodem -C 2125000000 -r 25 --rfsim;
	```

	If you want to restart the UE: Type "Control-C" to kill the process, then execute the last command again to restart.
	
	
1.  Verify that the OAI components interact with XRAN. In the ONOS console run:

	```
	hosts
	```

	This time you should see information associated with the simulated OAI UE.

1.  Start up an `iperf` server on the simulated UE. Open another shell on the `sim-ran` node and run:

	```
	iperf -s -u -i 1
	```
	
1. Start up an `iperf` client on the EPC. Open another shell on the `epc` node and run:

	```
	iperf -c 192.168.0.2 -u -i 1 -b 5M -t 5
	```
	
1. Note the throughput reported in the iperf *server* node. (I.e., the output for iperf on the simulated UE on the `sim-ran` node.)

1.  Change the number of physical resource blocks (PRBs) in the downlink direction via the XRAN API. On the `epc` node execute: 

	```
	curl -H "Content-Type: application/json" --request PATCH -d '{"RRMConf":{"start_prb_dl":0,"end_prb_dl":10}}' -u onos:rocks "http://localhost:8181/onos/xran/links/{00001000},{0}"
	```

1. Rerun the `iperf` test and note the change in throughput. On the `epc` node, rerun:

	```
	iperf -c 192.168.0.2 -u -i 1 -b 5M -t 5
	```
	
1. Note the throughput reported in the iperf *server* node. (I.e., the output for iperf on the simulated UE on the `sim-ran` node.)

	You should observe a lower downlink throughput than before.

1.  When you are finished, return to the experiment page and press the `Terminate` button.
